class CreateUsers < ActiveRecord::Migration[5.2]
  def change
    create_table :users do |t|
      t.string :username, null: false, unique: true
      t.string :email, null: false, index: true, unique: true
      # TODO -- filter this field on request
      t.string :password

      t.timestamps
    end
  end
end
