Rails.application.routes.draw do

  get 'user/me' => 'users#show'
  post 'user/new' => 'users#create'
  put 'user/edit' => 'users#update'
  delete '/user' => 'users#delete'

  resources :projects, except: :destroy
  put 'projects/:id/follow', to: 'projects#follow'
  post 'projects/:id/favorite', to: 'projects#favorite_some_graphic'
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
