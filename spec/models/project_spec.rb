require 'rails_helper'

RSpec.describe Project, type: :model do
  let(:project_params) {
    {
        name: 'testing',
        repo_link: 'a_valid_link',
        repo_id: 42
    }
  }

  describe "Validations" do

    context "It works" do
      it "with name and repo_name given" do
        expect(Project.new project_params).to be_valid
      end
    end

    context "It breaks" do
      it "without a name" do
        project_params.delete :name
        expect(
            (Project.create project_params).errors.messages
        ).to have_key :name
      end

      it "without a repo_link" do
        project_params.delete :repo_link
        expect(
            (Project.create project_params).errors.messages
        ).to have_key :repo_link
      end

      it "without a repo_id" do
        project_params.delete :repo_id
        expect(
            (Project.create project_params).errors.messages
        ).to have_key :repo_id
      end

      it "with a float repo_link" do
        project_params[:repo_id] = 42.7
        expect(
            (Project.create project_params).errors.messages
        ).to have_key :repo_id
      end

      it "with a negative repo_link" do
        project_params[:repo_id] = -42
        expect(
            (Project.create project_params).errors.messages
        ).to have_key :repo_id
      end

      it "with a repeated repo_link" do
        Project.create project_params
        expect(
            (Project.create project_params).errors.messages
        ).to have_key :repo_link
      end

      it "with a repeated repo_id" do
        Project.create project_params
        expect(
            (Project.create project_params).errors.messages
        ).to have_key :repo_id
      end
    end
  end

  context "Relationships" do
    # it "should have channels method" do
    #   expect(Project.new project_params).to respond_to :channels
    # end

    it "should have followers (users) method" do
      expect(Project.new project_params).to respond_to :users
    end
  end
end
