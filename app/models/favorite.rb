class Favorite < ApplicationRecord
  belongs_to :user
  belongs_to :project

  validates :graph, uniqueness: { scope: [:user, :project] }
end
