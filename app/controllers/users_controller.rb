class UsersController < ApplicationController
  before_action :authenticate, except: :create

  def create
    @user = User.new user_params
    if @user.save
      render json: @user, status: :created
    else
      render json: @user.errors, status: :unprocessable_entity
    end
  end

  def show
    if @user
      render json: @user, status: :ok
    else
      render status: :not_acceptable
    end
  end

  def destroy
    @user.delete
    render json: { message: 'User was deleted' }
  end

  private

  # Setting up strict parameters for when we add account creation.
  def user_params
    params.require(:user).permit(:username, :email, :password)
  end
end
