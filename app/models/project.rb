class Project < ApplicationRecord
  has_and_belongs_to_many :users
  has_many :favorites

  validates :name, :repo_link, presence: true
  validates :repo_link, uniqueness: { scope: :name }
  validates :repo_id, numericality: {
      only_integer: true,
      greater_than: 0}
  validates :repo_id, uniqueness: { scope: :name}

  # TODO -- Check out for other necessary validations for IRC channels and Issue trackers
end
