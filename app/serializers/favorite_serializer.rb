class FavoriteSerializer < ActiveModel::Serializer
  attributes :project_id, :project, :graph
end
