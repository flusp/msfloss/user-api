require 'rails_helper'

RSpec.describe User, type: :model do
  let(:user_params) {
    {
        email: 'test@user.uk',
        password: '12345678',
        username: 'Tester06'
    }
  }

  describe "Validations" do

    context "When email and password are valid" do
      it "it works" do
        expect(User.new user_params).to be_valid
      end
    end

    context "It breaks" do
      it "without an email" do
        user_params.delete :email
        expect(
            (User.create user_params).errors.messages
        ).to have_key :email
      end

      it "without a password" do
        user_params.delete :password
        expect(
            (User.create user_params).errors.messages
        ).to have_key :password
      end

      it "without a username" do
        user_params.delete :username
        expect(
            (User.create user_params).errors.messages
        ).to have_key :username
      end

      it "with an invalid email" do
        user_params[:email] = 'fail@test'
        expect(
            (User.create user_params).errors.messages
        ).to have_key :email
      end

      it "with a repeated email" do
        User.create user_params
        user_params[:username] = 'BlindMonkey2112'
        expect(
            (User.create user_params).errors.messages
        ).to have_key :email
      end

      it "with a repeated username" do
        User.create user_params
        user_params[:email] = 'another@email.com'
        expect(
            (User.create user_params).errors.messages
        ).to have_key :username
      end

      it "with a short password" do
        user_params[:password] = '56890'
        expect(
            (User.create user_params).errors.messages
        ).to have_key :password
      end

      it "with a long password" do
        user_params[:password] = 'abcdefghijklmnopqrstuvwxyz01234567890abcdefghijklmnopqrstuvwxyz01234567890'
        expect(
            (User.create user_params).errors.messages
        ).to have_key :password
      end
    end
  end

  describe "Relationships" do

    context "Every user has " do
      it "many projects" do
        expect(User.new user_params).to respond_to :projects
      end
    end

  end
end
