module ProjectHelper
  def follow_a_project(user, project)
    project.users += [user] unless is_already_following?(user, project)
  end

  def unfollow_a_project(user, project)
    project.users -= [user]
  end

  def favorite_a_graphic(user, project, graphic)
    favorite = Favorite.find_by(user: user, project: project, graph: graphic)
    if favorite
      favorite.delete
      nil
    else
      fav = Favorite.create(user: user, project: project, graph: graphic)
    end
  end

  private

  def is_already_following?(some_user, project)
    project.users.include?(some_user)
  end
end
