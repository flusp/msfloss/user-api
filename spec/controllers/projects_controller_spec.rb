require 'rails_helper'

RSpec.describe ProjectsController, type: :controller do

  # This should return the minimal set of attributes required to create a valid
  # Project. As you add validations to Project, be sure to
  # adjust the attributes here as well.
  let(:valid_attributes) {
    {
        name: 'testing',
        repo_id: 42,
        repo_link: 'valid_link.com',
        description: 'Cant you see? It is just a test'
    }
  }

  let(:no_repo) {
    {
        name: 'testing',
        description: 'Cant you see? It is just a test'
    }
  }

  let(:user_params) {
    {
      email: 'user@domain.com',
      username: 'TestingThisBeauty',
      password: '123456789'
    }
  }

  let(:user) {User.create! user_params}
  let(:request_params) {
    {
        project: valid_attributes,
        email: user.email,
        password: user.password
    }
  }


  describe "GET #index" do
    it "returns a success response" do
      get :index
      expect(response).to have_http_status 200
    end
  end

  describe "GET #show" do
    it "returns a success response" do
      project = Project.create! valid_attributes
      get :show, params: {id: project.to_param}
      expect(response).to have_http_status 200
    end
  end

  describe "POST #create" do
    context "with valid params and authentication" do
      it "creates a new Project" do
        expect {
          post :create, params: request_params
        }.to change(Project, :count).by(1)
      end

      it "renders a response with status 200" do
        post :create, params: request_params
        expect(response).to have_http_status(:created)
      end

      it "should render a JSON response with new project" do
        post :create, params: request_params
        expect(response.content_type).to eq('application/json')
      end

      it "should point to last Project created" do
        post :create, params: request_params
        expect(response.location).to eq(project_url(Project.last))
      end
    end

    context "with invalid params" do
      it "request should have HTTP status 422 (unprocessable_entity)" do
        request_params[:project].delete :repo_link
        post :create, params: request_params
        expect(response).to have_http_status(:unprocessable_entity)
      end

      it "should not allow a POST without authentication" do
        post :create, params: {project: valid_attributes}
        expect(response).to have_http_status(:unauthorized)
      end
    end
  end

  describe "PUT #update" do

    let(:project) {Project.create! valid_attributes}
    let(:new_attributes) {
      {
          id: project.to_param,
          project: {
              name: 'not a real project'
          },
      }
    }

    let(:invalid_update) {
      {
          id: project.to_param,
          project: {
              name: ''
          },
      }
    }

    context "with valid params and authentication" do

      it "updates the requested project" do
        put :update, params: new_attributes
        expect(response).to have_http_status(:ok)
      end

      it "renders a JSON response with the project" do
        put :update, params: new_attributes
        expect(response.content_type).to eq('application/json')
      end
    end

    context "with invalid params" do
      it "request should have HTTP status 422 (unprocessable_entity)" do
        put :update, params: invalid_update
        expect(response).to have_http_status(:unprocessable_entity)
      end
    end
  end

  describe "PUT follow" do

    let(:project) {Project.create name: 'test', repo_link: 'my_link.com'}
    let(:pin_params) {
      {
          id: project.to_param,
          pin: 1, email: user.email,
          password: user.password
      }
    }
    let(:unpin_params) {
      {
          id: project.to_param,
          pin: 0, email: user.email,
          password: user.password
      }
    }

    context "when user is authenticated" do

      it "request should have status accepted (202)" do
        put :follow, params: pin_params
        expect(response).to have_http_status :accepted
      end

      it "should increase the number of followers of a project if project is not followed yet" do
        expect{
          put :follow, params: pin_params
        }.to change(project.users, :count).by(1)
      end

      it "should keep the number of followers of a project if project is already followed" do
        put :follow, params: pin_params
        expect{
          put :follow, params: pin_params
        }.to change(project.users, :count).by(0)
      end

      it "should decrease the number of followers of a project if project is followed" do
        put :follow, params: pin_params
        expect{
          put :follow, params: unpin_params
        }.to change(project.users, :count).by(-1)
      end

      it "should keep the number of followers of a project if project is not followed" do
        put :follow, params: unpin_params
        expect{
          put :follow, params: unpin_params
        }.to change(project.users, :count).by(0)
      end

      it "request should have status bad request (400) if pin is nil" do
        unpin_params.delete :pin
        put :follow, params: unpin_params
        expect(response).to have_http_status :bad_request
      end
    end

    context "when user isn't authenticated" do
      it "request should have status unauthorized (401)" do
        put :follow, params: {id: project.to_param, pin: 0}
        expect(response).to have_http_status :unauthorized
      end
    end
  end

end
