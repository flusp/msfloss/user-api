class CreateProjects < ActiveRecord::Migration[5.2]
  def change
    create_table :projects do |t|
      t.string :name
      t.string :repo_link
      t.integer :repo_id
      t.string :description, default: ""
      # TODO -- Is git a Float or an Integer?
      t.float :git_frequency, default: 0.0
      t.string :irc_channel, default: ""
      t.string :irc_server, default: ""
      t.string :irc_password, default: ""
      t.string :email_list_name, default: ""
      t.string :issue_client, default: ""
      t.string :issue_url, default: ""
      t.string :issue_project, default: ""
      t.string :issue_user, default: ""
      t.string :issue_password, default: ""
      t.string :issue_private_token,default: ""

      t.timestamps
    end
  end
end
