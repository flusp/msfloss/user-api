class ProjectsController < ApplicationController
  include ProjectHelper
  before_action :authenticate, only: [:create, :follow]
  before_action :set_project, only: [:show, :favorite,
                                     :update, :follow]

  # GET /projects
  def index
    @projects = Project.all
    render json: @projects
  end

  # GET /projects/1
  def show
    render json: @project
  end

  # POST /projects
  def create
    @project = Project.new(project_params)
    if @user && @project.save
      render json: @project, status: :created, location: @project
    elsif !@user
      render status: :unauthorized
    else
      render json: @project.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /projects/1
  def update
    if @project.update(project_params)
      render json: @project
    else
      render json: @project.errors, status: :unprocessable_entity
    end
  end

  def follow
    if !request['pin']
      render status: :bad_request
    else
      if request['pin'] == "1"
        follow_a_project(@user, @project)
      elsif request['pin'] == "0"
        unfollow_a_project(@user, @project)
      end
      render status: :accepted
    end
  end

  def favorite_some_graphic
    if !request['graphic_id']
      render json: {message: 'You have not specified a graphic'}, status: :bad_request
    else
      favorite = favorite_a_graphic(@user, @project, request['graphic_id'])
      if favorite
        render json: favorite, status: :ok
      else
        render status: :accepted
      end
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_project
      @project = Project.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def project_params
      params.require(:project).permit(:name, :repo_link,
                                      :repo_id, :description)
    end
end
