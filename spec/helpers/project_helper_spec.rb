require 'rails_helper'

# Specs in this file have access to a helper object that includes
# the ProjectHelper. For example:
#
# describe ProjectHelper do
#   describe "string concat" do
#     it "concats two strings with spaces" do
#       expect(helper.concat_strings("this","that")).to eq("this that")
#     end
#   end
# end
RSpec.describe ProjectHelper, type: :helper do

  let(:user_params) {
    {email: 'a@a.com', password: '12309876', username: 'tester'}
  }
  let(:project_params) {
    { name: 'test', repo_link: 'test_link.com', repo_id: 42 }
  }
  let(:user) { User.create! user_params }
  let(:project) { Project.create! project_params }

  describe "#follow_a_project" do

    context "when some user does not follow a project" do

      it "should increase the number of followed projects" do
        expect{
          follow_a_project(user, project)
        }.to change(project.users, :count).by(1)
      end
    end

    context "when some user follows a project" do

      it "should not increase the number of followed projects" do
        follow_a_project(user, project)
        expect{
          follow_a_project(user, project)
        }.to change(project.users, :count).by(0)
      end
    end
  end

  describe "#unfollow_a_project" do

    context "when some user follows a project" do
      it "should decrease the number of followed projects" do
        follow_a_project(user, project)
        expect{
          unfollow_a_project(user, project)
        }.to change(project.users, :count).by(-1)
      end
    end

    context "when some user does not follow a project" do
      it "should not decrease the number of followed projects" do
        expect{
          unfollow_a_project(user, project)
        }.to change(project.users, :count).by(0)
      end
    end
  end
end
