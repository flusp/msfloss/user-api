# User API

It is just a quick explanation about what is this project. If you want to know
how to use it, check on the 
[**wiki**](https://gitlab.com/flusp/msfloss/user-api/wikis/About-what-we-offer)

This project is about building an API to serve its sibling apps. Our focus is building 
authentication tools and ground for creating projects, channels and related features.

Information about the user interface can be obtained from [**Client React**](https://gitlab.com/flusp/msfloss/client-react)