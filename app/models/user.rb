class User < ApplicationRecord
  #Relationships
  has_and_belongs_to_many :projects
  has_many :favorites

  before_validation do
    (self.email = self.email.to_s.downcase) && (self.username = self.username.to_s.downcase)
  end

  validates :email, :username, presence: true
  validates :email, :username, uniqueness: true

  validates :email, format: { with: /\A([^@\s]+)@((?:[-a-z0-9]+\.)+[a-z]{2,})\Z/i}
  validates :password, length: {
      in: 8..72,
      too_short: 'Password must have more than 8 characters',
      too_long: 'Password must have less than 72 characters'
  }
end
