class ProjectSerializer < ActiveModel::Serializer
  attributes :id, :name, :repo_link, :description
end
